import {Injectable} from "angular2/core";
import {PURCHASEORDERS} from "../../components/purchase-order/mock-po";
import {PurchaseOrder} from "../../domain/purchase-order";

@Injectable()
export class POService {
    getPO() {
        return Promise.resolve(PURCHASEORDERS);
    }

    insertKnight(purchaseOrder: PurchaseOrder) {
        Promise.resolve(PURCHASEORDERS).then((purchaseOrders: PurchaseOrder[]) => purchaseOrders.push(purchaseOrder));
    }
}