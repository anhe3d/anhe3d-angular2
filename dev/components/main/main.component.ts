import {Component, OnInit} from 'angular2/core';
import {HTTPMealService} from "../../services/http-service/http-meal.service";
import {Router} from "angular2/router";


@Component({
    template: `
        <section>


            <paper-toolbar>
                <paper-icon-button icon="menu" paper-drawer-toggle></paper-icon-button>
                <span class="title">Main</span>
            </paper-toolbar>
            <div class="content">
                <h2>Welcome to AN-HE Three-Dimensional</h2>
                <img src="../assets/img/ANHE-LOGO.png" alt="ANHE-LOGO">
            </div>
            
          

            
          
            <footer>
                <p>© 2015-2016 AN-HE THREE-DIMENSIONAL ALL RIGHTS RESERVED</p>
            </footer>

        </section>
            
    
    `,
    styles: [`
        .content img{
            display: block;
            max-height: 350px;
            margin: 20px auto 20px auto;
            text-align: center;
        }
        .content h2{
            text-align: center;
            text-decoration: blink;
        }
        
       footer {
           margin-left: auto;
           margin-right: auto;
           text-align: center;
           font-size: 9px;
       }
        `]
})
export class MainComponent implements OnInit{
    created = false;

    constructor(private _httpMealService: HTTPMealService, private _router: Router) {};


    ngOnInit():any {
    }
    
    onClick() {
        this.created = true;
    }






















}