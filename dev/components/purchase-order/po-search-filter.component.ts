import {Component, EventEmitter, Output, OnInit} from "angular2/core";
import {Vendor} from "../../domain/vendor";
import {HTTPPurchaseService} from "../../services/http-service/http-purchase.service";
import {HTTPMealService} from "../../services/http-service/http-meal.service";
import {EmbeddedTemplateAst} from "angular2/compiler";
import {Employee} from "../../domain/employee";

@Component({
    selector: 'po-search-filter',
    template: `   
   
            <div class="toolbar" (click)="toggleFilters = !toggleFilters" [ngClass]="{open: toggleFilters}">
                Filters
                <iron-icon icon="filter-list"></iron-icon>
            </div>
            <div  class="filters" [ngClass]="{open: toggleFilters}">
                <div class="row">
                    <div class="dates col">
                        <vaadin-date-picker label="After" (value-changed)="filters.after = $event.detail.value; filtersChanged()"></vaadin-date-picker>
                        <vaadin-date-picker label="Before" (value-changed)="filters.before = $event.detail.value; filtersChanged()"></vaadin-date-picker>
                    </div>
                    <!--<div class="merchants col">-->
                        <!--<vaadin-combo-box class="merchants"></vaadin-combo-box>-->
                    <!--</div>-->
                </div>
                <div class="row">
                    <div class="amounts col">
                        <!--<paper-input placeholder="搜尋供應商" (keyup)="filters.coName = $event.target.value; filtersChanged();"></paper-input>-->
                        <vaadin-combo-box label="選擇供應商" [items]="vendors" item-label-path="vendorName" item-value-path="vendorName"
                                          (value-changed)="filters.coName = $event.detail.value; filtersChanged();"  [disabled]="disabled"></vaadin-combo-box>
                        
                        <vaadin-combo-box label="選擇負責人" [items]="employees" item-label-path="firstName" item-value-path="firstName"
                                          (value-changed)="filters.firstName = $event.detail.value; filtersChanged();"  [disabled]="disabled"></vaadin-combo-box>
                    </div>
                    <div class="checkboxes col">
                        <paper-checkbox [disabled]="executedDisabled" (change)="updateStatus($event); filtersChanged();" name="executed">未執行</paper-checkbox>
                        <paper-checkbox [disabled]="goodReceiptDisabled" (change)="updateStatus($event); filtersChanged();" name="goodReceipt">未到貨</paper-checkbox>
                        <paper-checkbox [disabled]="paymentDisabled" (change)="updateStatus($event); filtersChanged();" name="payment">未付款</paper-checkbox>
                    </div>
                </div>
            </div>
    `,
    styleUrls: ['../../../src/css/po-search-filter.component.css']
})

export class POSearchFilter implements OnInit{
    @Output() filtersChange = new EventEmitter();
    
    filters: any = {};
    employees: Employee[];
    vendors: Vendor[];
    

    executedDisabled: boolean = false;
    goodReceiptDisabled: boolean = false;
    paymentDisabled: boolean = false;

    constructor(private _httpMealService: HTTPMealService, private _httpPurchaseService: HTTPPurchaseService) {}

    ngOnInit():any {
        this._httpMealService.getEmployees().subscribe(
            data => {
                this.employees = data;
                // console.log(this.employees);
            },
            error => alert(error), //error case
            () => console.log("Finished") //pass a function when everything is completed
        );
        this._httpPurchaseService.getVendors().subscribe(
            data => {
                this.vendors = data;
                // console.log(this.vendors);
            },
            error => alert(error), //error case
            () => console.log("Finished") //pass a function when everything is completed
        );
        
    }

    private updateStatus(e) {
        const status = e.target.name;
        const toggle = e.target.checked;


        switch (status) {
            case "executed":
                if (toggle) {
                    this.goodReceiptDisabled = true;
                    this.paymentDisabled = true;
                    this.filters.status = {name: status, toggle: toggle};
                } else {
                    this.goodReceiptDisabled = false;
                    this.paymentDisabled = false;
                    this.filters.status = {name: status, toggle: toggle};
                }
                break;
            case "payment":
                if (toggle) {
                    this.goodReceiptDisabled = true;
                    this.executedDisabled = true;
                    this.filters.status = {name: status, toggle: toggle};

                } else {
                    this.goodReceiptDisabled = false;
                    this.executedDisabled = false;
                    this.filters.status = {name: status, toggle: toggle};

                }
                break;

            case "goodReceipt":
                if (toggle) {
                    this.executedDisabled = true;
                    this.paymentDisabled = true;
                    this.filters.status = {name: status, toggle: toggle};

                } else {
                    this.executedDisabled = false;
                    this.paymentDisabled = false;
                    this.filters.status = {name: status, toggle: toggle};

                }
                break;

        }
    }

    private filtersChanged() {
        this.filtersChange.emit(this.filters);
    }


}