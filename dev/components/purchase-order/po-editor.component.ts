import {Component, OnInit, EventEmitter, Output} from "angular2/core";
import {Employee} from "../../domain/employee";
import {HTTPPurchaseService} from "../../services/http-service/http-purchase.service";
import {Vendor} from "../../domain/vendor";
import {Item} from "../../domain/item";
import {PurchaseOrder} from "../../domain/purchase-order";
import {POItem} from "../../domain/po-item";
import {ItemUnit} from "../../domain/item-unit";
import {POStatus} from "../../domain/po-status";
@Component({
    selector: 'po-editor',
    template: `            
            <div class="main-layout">
                <h2>創建訂購單</h2>
                <paper-icon-button icon="close" dialog-dismiss></paper-icon-button>
            </div>
            <vaadin-combo-box label="選擇供應商" [items]="vendors" item-label-path="vendorName" item-value-path="vendorName"
                    (value-changed)="onVendorSelect($event)"  [disabled]="disabled"></vaadin-combo-box>
            <paper-button raised [disabled]="disabled" (click)="onNext()" >下一步</paper-button>
            <paper-button raised [disabled]="!disabled" (click)="onChangedVendor()">重選供應商</paper-button>
            
            <section *ngIf="disabled !== false">
                <div class="edit-layout">
                    <vaadin-combo-box label="員工名稱" [items]="employees" item-label-path="firstName" item-value-path="firstName"
                                      (value-changed)="onEmployeeSelected($event)" ></vaadin-combo-box>
                    <paper-input  style="padding-left: 20px" label="稅" type="number"
                                 [(ngModel)]="newPurchaseOrder.tax" ngDefaultControl >
                    </paper-input>
                </div>
                <div>
                    
                </div>
                <!--<div>-->
                    <!--<paper-input label="備註" type="text"-->
                                 <!--[(ngModel)]="newPurchaseOrder.remark" ngDefaultControl >-->
                    <!--</paper-input>-->
                <!--</div>-->
                
                
                
                <div class="edit-layout">
                    <vaadin-combo-box label="相關物料" [items]="items" item-label-path="itemName" item-value-path="itemPrice"
                                      (selected-item-changed)="onItemSelected($event)" ></vaadin-combo-box>
                    <paper-input label="數量" type="number" auto-validate allowed-pattern="^[0-9][0-9]*$" error-message="number only!"
                                 style="padding-left: 20px" [(ngModel)]="quantity" ngDefaultControl >
                    </paper-input>
                </div>
                
                <div class="edit-layout">
                    <p>物料料號： {{itemVendorNum}} </p>
                </div>
                <div class="edit-layout">
                        <p>單價： {{price}} </p>
                        <p style="margin-left: 20px">單位： {{itemUnit.unitName}}</p>
                    
                        <paper-fab mini rasied icon="add" (mousedown)="cleanUpPreEvent()" (mouseup)="addPoItem()"></paper-fab>
                        <paper-fab mini rasied icon="remove" (mousedown)="cleanUpPreEvent()" (mouseup)="removePoItem()"></paper-fab>
                    
                </div>
                <div>
                    <vaadin-grid #grid [items]="DisplayNewPoItems" selection-mode="multi" visible-rows="3" (selected-items-changed)="selected(grid)">
                        <table>
                            <colgroup>
                                <col name="item.itemName">
                                <col name="quantity">
                                <col name="price">
                                <col name="item.itemVendorNum">
                            </colgroup>
                        </table>
                    </vaadin-grid>
                </div>
                
                
            <paper-button raised class="submit-button"
                    [disabled]="selectedEmployee.firstName === null || selectedItem.itemName === null || quantity <= 0"  
                    (mousedown)="onSubmitPo()" (click)="onSubmitPoItem()">Submit
            </paper-button>
            
            </section>
            
            

            `,
    styleUrls: ['../../../src/css/po-editor.component.css'],
    providers: [HTTPPurchaseService]
    
})
export class POEditor implements OnInit{
    @Output() refresh = new EventEmitter();
    
    vendors: Vendor[];
    employees: Employee[];
    items: Item[];
    temp: POItem[] = [];
    temp2: POItem[] = [];
    temp3: POItem[] = [];

    selectedVendor: Vendor = new Vendor;
    selectedItem: Item = new Item;
    selectedEmployee: Employee = new Employee;

    DisplayNewPoItems: POItem[] = [];
    newPoItems: POItem[] = [];
    price: number;
    itemVendorNum: string;
    itemUnit: ItemUnit = new ItemUnit;
    quantity: number;
    selection: number[];

    disabled = false;

    public newPurchaseOrder: PurchaseOrder = new PurchaseOrder();

    constructor(private _httpPurchaseService: HTTPPurchaseService) {}

    ngOnInit():any {
        this._httpPurchaseService.getVendors().subscribe(
            data => {
                this.vendors = data;
                console.log(this.vendors);
            },
            error => alert(error), //error case
            () => console.log("Finished") //pass a function when everything is completed
        );

        this._httpPurchaseService.getEmployees().subscribe(
            data => {
                this.employees = data;
                console.log(this.employees);
            },
            error => alert(error),
            () => console.log("getEmployees Finished")
        );

    }

    onVendorSelect(e) {
        console.log(e.detail.value);
        this.selectedVendor.vendorName = e.detail.value;
    }

    onNext() {
        console.log(this.selectedVendor.vendorName);

        if (this.selectedVendor.vendorName !== null) {
            this._httpPurchaseService.getItemsByVendor(this.selectedVendor.vendorName).subscribe(
                data => {
                    this.items = data;
                    console.log(this.items);
                },
                error => alert(error),
                () => console.log("getItems Finished")
            );
            this.disabled = true;
        }
    }

    onChangedVendor() {
        this.disabled = false;
        this.selectedEmployee = new Employee;
        this.selectedItem = new Item;
        this.newPurchaseOrder = new PurchaseOrder();
        this.price = null;
        this.itemVendorNum = null;
        this.itemUnit = new ItemUnit;
        this.quantity = null;
        this.temp = [];
        this.temp3 = [];
        this.DisplayNewPoItems = [];
        this.newPoItems = [];
    }

    onEmployeeSelected(e) {
        console.log(e.detail.value);
        this.selectedEmployee.firstName = e.detail.value;
    }
    
    onItemSelected(selection) {
        //in case of selection.detail is null
        if (selection.detail.value !== null){
            this.selectedItem = selection.detail.value;
            this.price = selection.detail.value.itemPrice;
            this.itemVendorNum = selection.detail.value.itemVendorNum;
            this.itemUnit = selection.detail.value.itemUnit;
        }
    }
    
    addPoItem() {
        console.log("addPoItem");

        if (this.selectedItem.itemName !== null && this.quantity > 0) {
            if (this.temp.length > 0 && (this.selectedItem.itemId !== this.temp[this.temp.length-1].item.itemId)) {
                this.temp3.push(new POItem(this.selectedItem, this.quantity, this.price*this.quantity));
                this.temp.push(new POItem(this.selectedItem, this.quantity, this.price*this.quantity));
                // console.log(new POItem(this.selectedItem, this.quantity, this.price*this.quantity));
                // this.DisplayNewPoItems.push(new POItem(this.selectedItem, this.quantity, this.price*this.quantity));


                // console.log(this.temp3[0]);
                // console.log(this.DisplayNewPoItems);
                // console.log(this.temp[0]);
                console.log("***************");

            } else if(this.temp.length === 0) {
                this.temp3.push(new POItem(this.selectedItem, this.quantity, this.price*this.quantity));
                this.temp.push(new POItem(this.selectedItem, this.quantity, this.price*this.quantity));
                // console.log(new POItem(this.selectedItem, this.quantity, this.price*this.quantity));
                // this.DisplayNewPoItems.push(new POItem(this.selectedItem, this.quantity, this.price*this.quantity));

                // console.log(this.temp3[0]);
                // console.log(this.DisplayNewPoItems);
                // console.log(this.temp[0]);
                console.log("***************");

            }
        }
        this.newPoItems = this.temp3;
        this.DisplayNewPoItems = this.temp;

        console.log(this.newPoItems);
        console.log(this.DisplayNewPoItems);

    }
    
    removePoItem() {
        // console.log("removePoItem");
        if(this.selection !== undefined) {
            if(this.selection.length > 0) {
                for(let i=this.selection.length-1; i>=0; i--) {
                    console.log(i);
                    this.temp.splice(this.selection[i], 1);
                    this.temp3.splice(this.selection[i], 1);
                }
            }
            this.DisplayNewPoItems = this.temp;
            this.newPoItems = this.temp3;
            this.selection = [];
        }
    }
    
    cleanUpPreEvent() {
        // console.log("cleanUpPreEvent");
        this.DisplayNewPoItems = this.temp2;
    }
    
    onSubmitPo() {
        console.log("onSubmitPo");
        this.newPurchaseOrder.employee = this.employees.filter((employee: Employee) => employee.firstName === this.selectedEmployee.firstName)[0];
        this.newPurchaseOrder.vendor = this.vendors.filter((vendor: Vendor) => vendor.vendorName === this.selectedVendor.vendorName)[0];
        this.newPurchaseOrder.poStatus = new POStatus();
        this.DisplayNewPoItems.map(element => {
            this.newPurchaseOrder.amount += element.price;
        });

        if (this.newPurchaseOrder.tax !== null || this.newPurchaseOrder.tax !== 0) {
            this.newPurchaseOrder.amount += this.newPurchaseOrder.amount*(this.newPurchaseOrder.tax);
            // console.log(this.newPurchaseOrder.amount);
        }
        console.log("~~~~~~~~~~~~~~~~~~~~~~~~this.DisplayNewPoItems: ");
        console.log(this.DisplayNewPoItems);

        this._httpPurchaseService.postPurchaseOrder(this.newPurchaseOrder, this.newPoItems).subscribe(
            data => {
                console.log(data);
            },
            error => alert(error),
            () => console.log("post Test Finished")
        );




        console.log("*********************");
        console.log(this.newPurchaseOrder);

    }
    
    onSubmitPoItem() {
        console.log("onSubmitPoItem");
        console.log(this.DisplayNewPoItems);

        // if (this.DisplayNewPoItems[0].purchaseOrder === null) {
        //     console.log("2222222222");
        //     console.log(this.newPurchaseOrder);
        //     this.DisplayNewPoItems.forEach(poItem => {
        //         console.log("poItem:   "+poItem);
        //         poItem.purchaseOrder = this.newPurchaseOrder;
        //         this.postPoItem(poItem)
        //     });
        // }


        //initialize every variable
        this.disabled = false;
        this.selectedEmployee = new Employee;
        this.selectedItem = new Item;
        this.newPurchaseOrder = new PurchaseOrder;
        this.price = null;
        this.itemVendorNum = null;
        this.itemUnit = new ItemUnit;
        this.quantity = null;
        this.temp = [];
        this.temp3 = [];
        this.DisplayNewPoItems = [];
        this.newPoItems = [];
        this.refresh.emit("");

    }
    
    selected(grid) {
        // console.log("------------selected");

        if (grid.selection.selected().length > 0) {
            // grid.selection.clear();
            this.selection = grid.selection.selected();
            // console.log("selected: "+grid.selection.selected());
            // console.log("deselected: "+grid.selection.deselected());
        }
    }
    
    
}