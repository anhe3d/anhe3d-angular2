import {Component} from "angular2/core";
import {POList} from "./po-list.component";
import {POEditor} from "./po-editor.component";
import {PODetail} from "./po-detail.component";

@Component({
    selector: 'po-overview-page',
    template: `

        <paper-toolbar>
            <paper-icon-button icon="menu" paper-drawer-toggle></paper-icon-button>
            <span class="title">Purchase Order</span>
            <paper-icon-button icon="refresh" raised (click)="poList.refreshItems()"></paper-icon-button>
            <paper-icon-button icon="add" raised (click)="creatDialog.toggle()"></paper-icon-button>
        </paper-toolbar>
        <div class="content">
            <po-list #poList (viewDetail)="poDetail.selectedOrder = $event; detailDialog.open(); poDetail.setDetail()"></po-list>
        </div>
        
        
        
        <paper-dialog #creatDialog id="creatDialog" modal>
            <po-editor (refresh)="creatDialog.close(); poList.refreshItems()"></po-editor>
        </paper-dialog>
        <paper-dialog #detailDialog id="detailDialog" modal>
            <po-detail #poDetail (closeDialog)="detailDialog.close();" ></po-detail>
        </paper-dialog>
        
       
`,
    styles: [`
      :host {
        display: flex;
        flex-direction: column;
        height: 100%;
      }
      .content {
        display: flex;
        flex: 1;
        
      }
      po-list {
        flex: 1;
        width: 100%;
      }
      h1 {
        font-weight: 300;
      }
      
      paper-dialog {
        display: block;
        padding: 16px 16px;
        border: 1px solid #ccc;
        position: absolute;
        top: 0;
        margin: 0;
        width: 70%;
        height: 100vh;
      }
      
      /*po-editor {*/
        /*display: flex;*/
        /*flex-direction: column;*/
        /*height: 100%;*/
        /*margin: 0 !important;*/
        /*padding: 0 !important;*/
      /*}*/
      @media (max-width: 600px) {
        paper-dialog {
          width: 100vw;
        }
      }
      @media (max-width: 600px) {
        h1 {
          font-size: 18px;
        }
      }
    `],
    directives: [POList, POEditor, PODetail]
})
export class POOverviewPage {


}
