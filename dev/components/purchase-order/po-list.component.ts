import {Component, OnInit, Output, EventEmitter} from "angular2/core";
import {POSearchFilter} from "./po-search-filter.component";
import {PurchaseOrder} from "../../domain/purchase-order";
import {HTTPPurchaseService} from "../../services/http-service/http-purchase.service";
declare var moment: any;

@Component({
    selector: 'po-list',
    template: `


        <po-search-filter (filtersChange)="filters=$event; onFilter($event);"></po-search-filter>
        <vaadin-grid #grid  [items]="displayedPurchaseOrders" [frozenColumns]="1" 
                     (selected-items-changed)="selected(grid)" (value-changed)="onChange($event)"
                     >
                     <!--(sort-order-changed)="sortItem($event)"-->
              <table>
                
                <colgroup>
                  <col name="createTime" sortable sort-direction="desc">
                  <col name="vendor.vendorName" sortable >
                  <col name="employee.firstName" sortable >
                  <col name="amount" sortable >
                  <col name="poStatus.approved">
                  <col name="poStatus.executed">
                  <col name="poStatus.goodReceipt">
                  <col name="poStatus.payment">
                  <col name="poStatus.invoiceReceipt">
                  <col name="poStatus.completed">
                </colgroup>

                  <thead>
                  <tr>
                      <th>建單時間</th>
                      <th>供應商</th>
                      <th>負責人</th>
                      <th>總價</th>
                  </tr>
                  </thead>

                  
              </table>
              
        </vaadin-grid>
        `,
    styleUrls: ['../../../src/css/po-list.component.css'],
    directives: [POSearchFilter],
    providers: [HTTPPurchaseService]
})

export class POList implements OnInit{
    @Output() viewDetail = new EventEmitter();

    purchaseOrders: PurchaseOrder[];
    displayedPurchaseOrders: PurchaseOrder[];

    sortOrder: Object;
    filters: Object;

    constructor(private _httpPurchaseService: HTTPPurchaseService) {};

    ngOnInit():any {
         this.refreshItems();
    }

    onFilter(event: any) {

        const filterVendor: string = event["vendorName"];
        const filterEmployee: string = event["firstName"];
        const statusText: Object = event["status"];
        const after: Date = event["after"];
        const before: Date = event["before"];

        // console.log(filterText);
        // console.log(statusText);
        // console.log(after);
        // console.log(before);

        this.displayedPurchaseOrders = this.purchaseOrders.filter((purchaseOrder: PurchaseOrder) =>
            !filterVendor || purchaseOrder.vendor.vendorName.indexOf(filterVendor) > -1
        );
        this.displayedPurchaseOrders = this.displayedPurchaseOrders.filter((purchaseOrder: PurchaseOrder) =>
            !filterEmployee || purchaseOrder.employee.firstName.indexOf(filterEmployee) > -1
        );



        if (statusText) {
            switch (statusText["name"]) {
                case "executed":
                    if(statusText["toggle"]) {
                        this.displayedPurchaseOrders = this.displayedPurchaseOrders.filter((purchaseOrder: PurchaseOrder) =>
                            purchaseOrder.poStatus.executed !== statusText["toggle"]
                        );
                    }
                    break;
                case "payment":
                    if(statusText["toggle"]) {
                        this.displayedPurchaseOrders = this.displayedPurchaseOrders.filter((purchaseOrder: PurchaseOrder) =>
                            purchaseOrder.poStatus.payment !== statusText["toggle"]
                        );
                    }
                    break;
                case "goodReceipt":
                    if(statusText["toggle"]) {
                        this.displayedPurchaseOrders = this.displayedPurchaseOrders.filter((purchaseOrder: PurchaseOrder) =>
                            purchaseOrder.poStatus.goodReceipt !== statusText["toggle"]
                        );
                    }
                    break;
            }
        }

        if (after) {
            this.displayedPurchaseOrders = this.displayedPurchaseOrders.filter((purchaseOrder: PurchaseOrder) =>
                <Date><any>purchaseOrder.createTime >= after
            );
        }

        if (before) {
            this.displayedPurchaseOrders = this.displayedPurchaseOrders.filter((purchaseOrder: PurchaseOrder) =>
                <Date><any>purchaseOrder.createTime <= before
            );
        }



    }
    
    sortItem(event: any) {
        const grid = event.target;
        const sortOrder = grid.sortOrder[0];
        const sortProperty = grid.columns[sortOrder.column].name;
        const sortDirection = sortOrder.direction;
        this.purchaseOrders.sort((a, b) => {
            let res: number;
            let valueA: string = grid.get(sortProperty, a),
                valueB: string = grid.get(sortProperty, b);
            if (!isNaN(<number><any>valueA)) {
                res = parseInt(valueA, 10) - parseInt(valueB, 10);
            } else {
                res = valueA.localeCompare(valueB);
            }
            if (sortDirection === 'desc') {
                res *= -1;
            }
            return res;
        });
    }

    private selected(grid) {
        var selection = grid.selection.selected();
        if (selection.length === 1) {
            grid.selection.clear();
            grid.getItem(selection[0], (err, item) => {
                this.viewDetail.emit(item);
            });
        }
    }
    
    refreshItems() {
        console.log("refreshItems");
        this._httpPurchaseService.getPurchaseOrders().subscribe(
            data => {
                this.purchaseOrders = data;
                // this.displayedPurchaseOrders = JSON.parse(JSON.stringify(this.purchaseOrders));
                this.displayedPurchaseOrders = this.purchaseOrders;
                for (let i in this.displayedPurchaseOrders) {
                    // console.log(this.purchaseOrders[i].createTime);
                    // console.log(moment(this.purchaseOrders[i].createTime ).format('LLLL'));
                    this.displayedPurchaseOrders[i].createTime = moment(this.displayedPurchaseOrders[i].createTime).format('YYYY-MM-DD');
                    // this.displayedPurchaseOrders[i].poStatus.approved;
                    // console.log(moment(this.purchaseOrders[i].createTime ).format('LLLL'));
                    // console.log(this.displayedPurchaseOrders[i].createTime);

                }
                // console.log(this.purchaseOrders);
            },
            error => alert(error)
        );


    }

    onChange(e) {
        console.log(e);
    }


    
}