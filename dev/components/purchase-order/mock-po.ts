//pretend to be the data from database
import {PurchaseOrder} from "../../domain/purchase-order";

export const PURCHASEORDERS: PurchaseOrder[] = [
    // new PurchaseOrder("A.co", "John", 0.05, 10000,"2016/06/12"),
    // new PurchaseOrder("B.co", "Amy", 0.05, 1000,"2015/07/12"),
    // new PurchaseOrder("C.co", "Jack", 0.05, 20000,"2016/07/22"),
    // new PurchaseOrder("D.co", "Kevin", 0.05, 30000,"2014/12/12"),
    // new PurchaseOrder("E.co", "ALice", 0.05, 300000,"2015/10/02")
    // {vendor: "A.co", employee: "John", tax: 0.05, amount: 10000, createTime: "2016/06/12",
    //     createdStatus: true, approvedStatus: false, executedStatus: false,
    //     goodReceiptStatus: false, paymentStatus: false, invoiceReceiptStatus: false, completedStatus: false},
    // {vendor: "B.co", employee: "Amy", tax: 0.05, amount: 1000, createTime: "2015/07/12",
    //     createdStatus: true, approvedStatus: true, executedStatus: false,
    //     goodReceiptStatus: false, paymentStatus: false, invoiceReceiptStatus: false, completedStatus: false},
    // {vendor: "C.co", employee: "Jack", tax: 0.05, amount: 20000, createTime: "2016/07/22",
    //     createdStatus: true, approvedStatus: true, executedStatus: true,
    //     goodReceiptStatus: false, paymentStatus: false, invoiceReceiptStatus: false, completedStatus: false},
    // {vendor: "D.co", employee: "Kevin", tax: 0.05, amount: 30000, createTime: "2014/12/12",
    //     createdStatus: true, approvedStatus: true, executedStatus: true,
    //     goodReceiptStatus: true, paymentStatus: true, invoiceReceiptStatus: false, completedStatus: false},
    // {vendor: "E.co", employee: "Alice", tax: 0.05, amount: 300000, createTime: "2015/10/02",
    //     createdStatus: true, approvedStatus: true, executedStatus: true,
    //     goodReceiptStatus: true, paymentStatus: true, invoiceReceiptStatus: true, completedStatus: false}
];

